import 'dart:convert';

import 'package:agent_process_app/main.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:agent_process_app/utils/myUtils.dart' as myUtils;
import 'package:shared_preferences/shared_preferences.dart';

class NightMode extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SwitchNightModel();
  }
}

class SwitchNightModel extends State<NightMode> {
  bool darkMode = false;

  checkMode() async {
    bool res;
    try {
      await myUtils.LocalStorage.getLocalStorageInstance()
          .then((SharedPreferences sharedPreferences) {
        res = sharedPreferences.getBool("themeData_dark");
      });
      if(res==null){
        res=false;
      }
    } catch (e) {
      res = false;
    }
    setState(() {
      darkMode = res;
    });
  }

  @override
  void initState() {
    super.initState();
    //判断当前是否为夜间模式
    checkMode();
  }

  changeMode(bool value) async {
    //value是打开或关闭状态
    print(value);

    await myUtils.LocalStorage.getLocalStorageInstance()
        .then((SharedPreferences sharedPreferences) {
      sharedPreferences.setBool("themeData_dark", value);
    });

//            MyAppScreen(true).getThemeData();

    setState(() {
      darkMode = value;
    });

//    await showDialog(
//        context: context,
//        builder: (_) => new AlertDialog(
//                title: new Text("操作通知"),
//                content: Text("主题切换成功，下次打开App时生效！"),
//                actions: <Widget>[
//                  new FlatButton(
//                    child: new Text("我知道了"),
//                    onPressed: () {
//                      Navigator.of(context).pop();
//                    },
//                  )
//                ]));
  }

  @override
  Widget build(BuildContext context) {
    return Switch(
        value: darkMode,
//                activeColor: Colors.blue,
        onChanged: (value) {
          changeMode(value);
        });
  }
}
