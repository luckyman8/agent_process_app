import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:agent_process_app/utils/myUtils.dart' as myUtils;
import 'package:shared_preferences/shared_preferences.dart';

class MineScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    logout() async {
      SharedPreferences s = await SharedPreferences.getInstance();
      s.setString("token", "");
      Navigator.of(context)
          .pushNamedAndRemoveUntil("/loginScreen", (route) => route == null);
    }

    getData() async {
      String token1;
      await myUtils.LocalStorage.getLocalStorageInstance()
          .then((SharedPreferences sharedPreferences) {
        token1 = sharedPreferences.getString("token");
      });
      print("token1=");
      print(token1);
      String token;
      await myUtils.LocalStorage.getLocalStorageInstance()
          .then((SharedPreferences sharedPreferences) {
        token = sharedPreferences.getString("token");
      });

      var str = "Hello world";
      var bytes = utf8.encode(str);

      var encoded1 = base64Encode(bytes);

      print(encoded1);
      print(base64Decode(encoded1));
      print(String.fromCharCodes(base64Decode(encoded1)));
      Dio dio = myUtils.MyDio.getDio(token);
      FormData formData = new FormData.from({
        "username": encoded1,
      });
      Response response;
      try {
        response = await dio.post("/login", data: formData);
        print(response.statusCode);
        print(response.data);
      } catch (e) {
        print(e);
        print("网络错误!");
      }
    }

    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("我的"),
          centerTitle: true,
        ),
        body: Column(
          children: <Widget>[
            new Container(
              padding: EdgeInsets.all(40.0),
              child: new Center(
                  child: Column(
                    children: <Widget>[
                      Icon(
                        Icons.account_circle,
                        size: 80.0,
                        color: Colors.blue,
                      ),
                      Text("超级管理员")
                    ],
                  )
              ),
            ),
            ListTile(
              leading: Icon(Icons.power_settings_new,color: Colors.red,size: 30.0,),
              title: Text('退出'),
              trailing: Icon(Icons.keyboard_arrow_right),
              onTap: (){
                logout();
              },
            ),
            new Divider(),
            new Container(
              child: RaisedButton(
                onPressed: () {
                  getData();
                },
                child: Text("获取数据"),
              ),
            ),
          ],
        ));
  }
}
