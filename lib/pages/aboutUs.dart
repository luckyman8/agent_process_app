import 'package:agent_process_app/utils/myUtils.dart' as myUtils;
import 'package:flutter/material.dart';

//const ListTile({
//Key key,
//this.leading,              // item 前置图标
//this.title,                // item 标题
//this.subtitle,             // item 副标题
//this.trailing,             // item 后置图标
//this.isThreeLine = false,  // item 是否三行显示
//this.dense,                // item 直观感受是整体大小
//this.contentPadding,       // item 内容内边距
//this.enabled = true,
//this.onTap,                // item onTap 点击事件
//this.onLongPress,          // item onLongPress 长按事件
//this.selected = false,     // item 是否选中状态
//})

class AboutUsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("关于我们"),
      ),
      body: Center(
          child: Text(
        "版本v${myUtils.AppInfo.version}",
      )),
    );
  }
}
