import 'package:flutter/material.dart';

//const ListTile({
//Key key,
//this.leading,              // item 前置图标
//this.title,                // item 标题
//this.subtitle,             // item 副标题
//this.trailing,             // item 后置图标
//this.isThreeLine = false,  // item 是否三行显示
//this.dense,                // item 直观感受是整体大小
//this.contentPadding,       // item 内容内边距
//this.enabled = true,
//this.onTap,                // item onTap 点击事件
//this.onLongPress,          // item onLongPress 长按事件
//this.selected = false,     // item 是否选中状态
//})

class StepScreen extends StatefulWidget {

    StepScreen(this.title);

    String title;

    @override
    State<StatefulWidget> createState() {
        return AgentStep(title);
    }

}

class AgentStep extends State<StepScreen> {

    AgentStep(this.title);

    String title;

    int currentStep = 0;
    List<Step> mySteps = [
        new Step(
                title: new Text("Step 1"),
                subtitle: Text("填写项目所在地"),
                content: TextField(),
                state: StepState.complete,
                isActive: true),
        new Step(
                title: new Text("Step 1"),
                subtitle: Text("填写项目所占面积"),
                content: TextField(),
                state: StepState.indexed,
                isActive: true),
        new Step(
                title: new Text("Step 2"),
                content: new Text("World"),
                state: StepState.disabled,
                isActive: false),
        new Step(
                title: new Text("Step 3"),
                content: new Text("Hello World"),
                state: StepState.disabled,
                isActive: false),
    ];

    @override
    Widget build(BuildContext context) {
        // TODO: implement build
        return Scaffold(
            appBar: AppBar(
                centerTitle: true,
                title: Text(title),
                    actions: <Widget>[
                    IconButton(
                        icon: Icon(Icons.list),
                        tooltip: "对接记录",
                        onPressed: () {},
                    ),
                    ],
            ),
            body: Stepper(
//                controlsBuilder: ,
                currentStep: this.currentStep,
                steps: mySteps,
                type: StepperType.vertical,
                onStepTapped: (step) {
                    setState(() {
                        currentStep = step;
                    });
                },
                onStepCancel: () {
                    setState(() {
                        if (currentStep > 0) {
                            currentStep = currentStep - 1;
                        } else {
                            currentStep = 0;
                        }
                    });
                },
                onStepContinue: () {
                    setState(() {
                        //这里需要判断下一步是否已启用
                        if (currentStep < mySteps.length - 1) {
                            currentStep = currentStep + 1;
                        } else {
                            currentStep = 0;
                        }
                    });
                },
            )
        );
    }
}
