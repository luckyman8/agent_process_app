import 'dart:io';

import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

//App的信息
class AppInfo {
    //App版本
    static final String version = "1.0.0";
}

class LocalStorage {
    //获取本地存储的实例
    static Future<SharedPreferences> getLocalStorageInstance() async {
        SharedPreferences sharedPreferencesInstance =
        await SharedPreferences.getInstance();
        return sharedPreferencesInstance;
    }
}

class MyDio {
    static String baseUrl = "http://192.168.19.178:8080";

//    static String baseUrl = "http://192.168.1.104:8080";
    static Dio getDio(String token) {
        Dio dio = new Dio();
        dio.options.baseUrl = baseUrl;
//        dio.options.connectTimeout = 10000;
//        dio.options.receiveTimeout = 5000;
//        dio.options.contentType = ContentType.json;
//        dio.options.responseType = ResponseType.json;
        dio.options.headers = {"token": token};
        return dio;
    }
}

class MyUtils {
    //打开某链接,打电话
    static launchURL(url) async {
        if (await canLaunch(url)) {
            await launch(url);
        } else {
//            throw 'Could not launch $url';
            print('Could not launch $url');
        }
    }

    //退出时清空一些内容
    static clearUserInfo() async {
        SharedPreferences s = await SharedPreferences.getInstance();
        bool themeData = s.getBool("themeData_dark");
        s.clear();
        s.setBool("themeData_dark", themeData);
    }

}


//setToken() async{
//  // 构建sp类
//  SharedPreferences sp = await SharedPreferences.getInstance();
//  // 存储
//  sp.setString("name", "lee");
//  sp.setInt("age", 24);
//  // 读取
//  sp.getString("name");// "lee"
//  print(sp.getString("a"));
//  sp.getInt("age");// 240
//  sp.get("name");// "lee"
//  //清除
//  sp.clear();
//  sp.remove("name");
//}

//main(){
//  getInstance().then((SharedPreferences sharedPreferences){
//    sharedPreferences.setString("a", "1");
//  });
//}
